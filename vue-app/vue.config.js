module.exports = {
  devServer: {
    public: '0.0.0.0:8080'
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/styles/_variables.scss";
          @import "@/assets/styles/_mixins.scss";
        `
      }
    }
  }
};
