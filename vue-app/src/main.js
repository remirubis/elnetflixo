import { createApp } from 'vue';
import { createStore } from 'vuex';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faMagnifyingGlass, faCaretDown, faPlay, faCircleInfo, faXmark, faStar, faHeart as HeartSolid
} from '@fortawesome/free-solid-svg-icons';
import {
  faHeart as HeartRegular
} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import App from './App.vue';
import router from './router';

library.add(faMagnifyingGlass, faCaretDown, faPlay, faCircleInfo, faXmark, faStar, HeartRegular, HeartSolid);

const app = createApp(App);
const store = createStore({
  state: {
    isConnected: localStorage.user,
    movieInfo: null,
    moviePlaying: null
  },
  mutations: {
    SET_USER(state, id) {
      // eslint-disable-next-line no-param-reassign
      state.isConnected = id;
    },
    SET_VIEW_MOVIE(state, movie) {
      // eslint-disable-next-line no-param-reassign
      state.movieInfo = movie;
    },
    SET_PLAY_MOVIE(state, movie) {
      // eslint-disable-next-line no-param-reassign
      state.moviePlaying = movie;
    }
  },
  actions: {
    setUser(context, id) {
      localStorage.user = id;
      context.commit('SET_USER', id);
    },
    removeUser(context) {
      delete localStorage.user;
      context.commit('SET_USER', null);
    },
    setMovieInfo(context, movie) {
      context.commit('SET_VIEW_MOVIE', movie);
    },
    removeMovieInfo(context) {
      context.commit('SET_VIEW_MOVIE', null);
    },
    playMovie(context, movie) {
      context.commit('SET_PLAY_MOVIE', movie);
    },
    stopMovie(context) {
      context.commit('SET_PLAY_MOVIE', null);
    }
  }
});

app.use(router);
app.component('FontAwesomeIcon', FontAwesomeIcon);
app.use(store);

app.mount('#app');
