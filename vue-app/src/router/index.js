import { createRouter, createWebHistory } from 'vue-router';
import Homepage from '../views/Homepage.vue';
import NotFound from '../views/NotFound.vue';
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import Categories from '../views/Categories.vue';
import Favories from '../views/Favories.vue';

const routes = [
  {
    path: '',
    alias: '/home',
    name: 'home',
    component: Homepage,
    meta: {
      title: 'Accueil'
    }
  },
  {
    path: '/connexion',
    name: 'login',
    component: Login,
    meta: {
      title: 'Connexion'
    }
  },
  {
    path: '/inscription',
    name: 'register',
    component: Register,
    meta: {
      title: 'Inscription'
    }
  },
  {
    path: '/categorie',
    name: 'categorie',
    component: Categories,
    meta: {
      title: 'Categorie'
    }
  },
  {
    path: '/favories',
    name: 'favories',
    component: Favories,
    meta: {
      title: 'Favorie'
    }
  },
  {
    path: '/oups',
    name: 'notFound',
    component: NotFound,
    meta: {
      title: 'Page introuvable'
    }
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/oups'
  }
];

const router = createRouter({
  history: createWebHistory(process.enBASE_URL),
  routes,
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active'
});

export default router;
