const express = require('express');

const CategoriesModel = require('../models/categories.model');

class CategoriesRouter {
  constructor(database) {
    this.router = express.Router();
    this.categoriesModel = new CategoriesModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/category', (req, res) => this.categoriesModel.add(req, res));
    this.router.patch('/category/:id', (req, res) => this.categoriesModel.updateById(req, res));
    this.router.delete('/category/:id', (req, res) => this.categoriesModel.removeById(req, res));
    this.router.get('/category/:id', (req, res) => this.categoriesModel.findById(req, res));
    this.router.get('/categories', (req, res) => this.categoriesModel.findAll(req, res));
    this.router.post('/categories/populate', (req, res) => this.categoriesModel.populate(req, res));
  };
}

module.exports = CategoriesRouter;
