const express = require('express');

const UsersFavoriesModel = require('../models/usersFavories.model');

class UsersFavories {
  constructor(database) {
    this.router = express.Router();
    this.usersFavoriesModel = new UsersFavoriesModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/favorie', (req, res) => this.usersFavoriesModel.add(req, res));
    this.router.patch('/favories/:id', (req, res) => this.usersFavoriesModel.updateById(req, res));
    this.router.delete('/favorie', (req, res) => this.usersFavoriesModel.remove(req, res));
    this.router.get('/favories/:id', (req, res) => this.usersFavoriesModel.findMoviesByUserId(req, res));
    this.router.get('/isUserFav', (req, res) => this.usersFavoriesModel.isUserFavMovie(req, res));
    this.router.get('/favories', (req, res) => this.usersFavoriesModel.findAll(req, res));
    this.router.post('/favories/populate', (req, res) => this.usersFavoriesModel.populate(req, res));
  };
}

module.exports = UsersFavories;
