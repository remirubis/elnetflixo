const express = require('express');

const ClassificationModel = require('../models/classifications.model');

class ClassificationRouter {
  constructor(database) {
    this.router = express.Router();
    this.classificationModel = new ClassificationModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/clasification', (req, res) => this.classificationModel.add(req, res));
    this.router.patch('/clasification/:id', (req, res) => this.classificationModel.updateById(req, res));
    this.router.delete('/clasification/:id', (req, res) => this.classificationModel.removeById(req, res));
    this.router.get('/clasification/:id', (req, res) => this.classificationModel.findById(req, res));
    this.router.get('/clasifications', (req, res) => this.classificationModel.findAll(req, res));
    this.router.post('/classifications/populate', (req, res) => this.classificationModel.populate(req, res));
  };
}

module.exports = ClassificationRouter;
