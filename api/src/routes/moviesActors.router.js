const express = require('express');

const MoviesActorsModel = require('../models/moviesActors.model');

class MoviesActors {
  constructor(database) {
    this.router = express.Router();
    this.moviesActorsModel = new MoviesActorsModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/movieActor', (req, res) => this.moviesActorsModel.add(req, res));
    this.router.patch('/movieActor', (req, res) => this.moviesActorsModel.updateNoteByMovieIdActorId(req, res));
    this.router.delete('/movieActor', (req, res) => this.moviesActorsModel.removeActorOfMovie(req, res));
    this.router.get('/movieActors/:movie', (req, res) => this.moviesActorsModel.findActorsByMovieId(req, res));
    this.router.get('/moviesActors', (req, res) => this.moviesActorsModel.findAll(req, res));
  };
}

module.exports = MoviesActors;
