const express = require('express');

const PlansModel = require('../models/plans.model');

class PlansRouter {
  constructor(database) {
    this.router = express.Router();
    this.plansModel = new PlansModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/plan', (req, res) => this.plansModel.add(req, res));
    this.router.patch('/plan/:id', (req, res) => this.plansModel.updateById(req, res));
    this.router.delete('/plan/:id', (req, res) => this.plansModel.removeById(req, res));
    this.router.get('/plan/:id', (req, res) => this.plansModel.findById(req, res));
    this.router.get('/plans', (req, res) => this.plansModel.findAll(req, res));
    this.router.post('/plans/populate', (req, res) => this.plansModel.populate(req, res));
  };
}

module.exports = PlansRouter;
