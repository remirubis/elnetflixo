const express = require('express');

const UsersWatchListModel = require('../models/usersWatchList.model');

class UsersWatchList {
  constructor(database) {
    this.router = express.Router();
    this.usersWatchListModel = new UsersWatchListModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/watchList', (req, res) => this.usersWatchListModel.add(req, res));
    this.router.patch('/watchList/:id', (req, res) => this.usersWatchListModel.updateById(req, res));
    this.router.delete('/watchList/:id', (req, res) => this.usersWatchListModel.removeById(req, res));
    this.router.get('/watchList/:id', (req, res) => this.usersWatchListModel.findByUserId(req, res));
    this.router.get('/mostViewed', (req, res) => this.usersWatchListModel.findMostViewed(req, res));
    this.router.get('/watchList', (req, res) => this.usersWatchListModel.findAll(req, res));
    this.router.post('/watchList/populate', (req, res) => this.usersWatchListModel.populate(req, res));
  };
}

module.exports = UsersWatchList;
