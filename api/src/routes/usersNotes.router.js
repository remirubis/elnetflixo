const express = require('express');

const UsersNotesModel = require('../models/usersNotes.model');

class UsersFavories {
  constructor(database) {
    this.router = express.Router();
    this.usersNotesModel = new UsersNotesModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/usersNote', (req, res) => this.usersNotesModel.add(req, res));
    this.router.patch('/usersNote', (req, res) => this.usersNotesModel.updateMovieNoteByUserId(req, res));
    this.router.delete('/usersNotes', (req, res) => this.usersNotesModel.removeMovieNoteByUserId(req, res));
    this.router.get('/movieNote/:id', (req, res) => this.usersNotesModel.getMovieNote(req, res));
    this.router.get('/movieNote', (req, res) => this.usersNotesModel.getUserNoteOfMovie(req, res));
    this.router.post('/usersNotes/populate', (req, res) => this.usersNotesModel.populate(req, res));
  };
}

module.exports = UsersFavories;
