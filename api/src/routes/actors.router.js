const express = require('express');

const ActorsModel = require('../models/actors.model');

class ActorsRouter {
  constructor(database) {
    this.router = express.Router();
    this.actorsModel = new ActorsModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/actor', (req, res) => this.actorsModel.add(req, res));
    this.router.patch('/actor/:id', (req, res) => this.actorsModel.updateById(req, res));
    this.router.delete('/actor/:id', (req, res) => this.actorsModel.removeById(req, res));
    this.router.get('/actor/:id', (req, res) => this.actorsModel.findById(req, res));
    this.router.get('/actors', (req, res) => this.actorsModel.findAll(req, res));
    this.router.post('/actors/populate', (req, res) => this.actorsModel.populate(req, res));
  };
}

module.exports = ActorsRouter;
