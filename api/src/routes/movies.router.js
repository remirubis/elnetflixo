const express = require('express');

const MoviesModel = require('../models/movies.model');

class MoviesRouter {
  constructor(database) {
    this.router = express.Router();
    this.moviesModel = new MoviesModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/movie', (req, res) => this.moviesModel.add(req, res));
    this.router.patch('/movie/:id', (req, res) => this.moviesModel.updateById(req, res));
    this.router.delete('/movie/:id', (req, res) => this.moviesModel.removeById(req, res));
    this.router.get('/movie/:id', (req, res) => this.moviesModel.findById(req, res));
    this.router.get('/movie/category/:id', (req, res) => this.moviesModel.findByCategoryId(req, res));
    this.router.get('/movies/recents', (req, res) => this.moviesModel.findRecentsMovies(req, res));
    this.router.get('/movies', (req, res) => this.moviesModel.findAll(req, res));
    this.router.get('/search', (req, res) => this.moviesModel.searchMovie(req, res));
    this.router.post('/movies/populate', (req, res) => this.moviesModel.populate(req, res));
  };
}

module.exports = MoviesRouter;
