const express = require('express');

const UsersModel = require('../models/users.model');

class UsersRouter {
  constructor(database) {
    this.router = express.Router();
    this.usersModel = new UsersModel(database);
    this.initRoutes();
  }

  initRoutes = () => {
    this.router.post('/user', (req, res) => this.usersModel.add(req, res));
    this.router.patch('/user/:id', (req, res) => this.usersModel.updateById(req, res));
    this.router.delete('/user/:id', (req, res) => this.usersModel.removeById(req, res));
    this.router.get('/user/:id', (req, res) => this.usersModel.findById(req, res));
    this.router.get('/password', (req, res) => this.usersModel.findPasswordByEmail(req, res));
    this.router.post('/hash', (req, res) => this.usersModel.hashPassword(req, res));
    this.router.get('/users', (req, res) => this.usersModel.findAll(req, res));
    this.router.post('/users/populate', (req, res) => this.usersModel.populate(req, res));
  };
}

module.exports = UsersRouter;
