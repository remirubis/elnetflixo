const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');

const Database = require('./models/database');
const UsersRouter = require('./routes/users.router');
const PlansRouter = require('./routes/plans.router');
const CategoriesRouter = require('./routes/categories.router');
const ActorsRouter = require('./routes/actors.router');
const MoviesRouter = require('./routes/movies.router');
const UsersWatchListRouter = require('./routes/usersWatchList.router');
const MoviesActorsRouter = require('./routes/moviesActors.router');
const ClassificationsRouter = require('./routes/classifications.router');
const UsersFavoriesRouter = require('./routes/usersFavories.router');
const UsersNotesRouter = require('./routes/usersNotes.router');

const app = express();
const apiPort = 9090;

// Database connection
const dbConnection = new Database();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet());
app.use(cors({
  exposedHeaders: ['Authorization'],
  origin: '*'
}));
app.use(bodyParser.json());

app.use('/api', [
  new PlansRouter(dbConnection).router,
  new CategoriesRouter(dbConnection).router,
  new ClassificationsRouter(dbConnection).router,
  new ActorsRouter(dbConnection).router,
  new MoviesRouter(dbConnection).router,
  new UsersRouter(dbConnection).router,
  new UsersWatchListRouter(dbConnection).router,
  new MoviesActorsRouter(dbConnection).router,
  new UsersFavoriesRouter(dbConnection).router,
  new UsersNotesRouter(dbConnection).router
]);

app.listen(apiPort);
