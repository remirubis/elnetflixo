const GlobalModel = require('./global.model');

const TABLE = 'users_watchlist';

class UsersWatchList extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_id INT NOT NULL,
        movie_id INT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE,
        FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE,
        UNIQUE KEY UNIQUE_LINK (user_id, movie_id)
      ) ENGINE=INNODB`,
      true
    );
  }

  findByUserId = async (req, res) => {
    const { id } = req.params;
    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT * FROM movies) p USING (movie_id) WHERE user_id = ?`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No data found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting data'
        }
      }));
  };

  findMostViewed = async (req, res) => {
    const { limit } = req.query;

    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }

    return this.sql.query(`
      SELECT movie_id, COUNT(*) AS occurences, p.*, c.name AS category_name, cl.title AS classification_title
      FROM ${TABLE}
      LEFT JOIN (SELECT * FROM movies) p USING (movie_id)
      LEFT JOIN (SELECT category_id, name FROM categories) c USING (category_id)
      LEFT JOIN (SELECT classification_id, title FROM classifications) cl USING (classification_id)
      GROUP BY ${TABLE}.movie_id
      ORDER BY occurences DESC
      ${limit ? `LIMIT ${limit}` : ''}`)
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No movie found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting movies'
        }
      }));
  };
}

module.exports = UsersWatchList;
