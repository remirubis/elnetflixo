const GlobalModel = require('./global.model');

const TABLE = 'users_notes';

class UsersNotes extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_id INT NOT NULL,
        movie_id INT NOT NULL,
        note INT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE,
        FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE,
        UNIQUE KEY UNIQUE_LINK (user_id, movie_id)
      ) ENGINE=INNODB`,
      true
    );
  }

  getUserNoteOfMovie = async (req, res) => {
    const { movie, user } = req.query;

    return this.sql.query(`SELECT note FROM ${TABLE} WHERE movie_id = ? AND user_id = ?`, [movie, user])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No notes found for this movie'
            }
          });
        }
        return res.status(200).json(data[0]);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during getting data from ${TABLE} table`
        }
      }));
  };

  getMovieNote = async (req, res) => {
    const { id } = req.params;

    return this.sql.query(`SELECT AVG(note) AS avg_note FROM ${TABLE} WHERE movie_id = ?`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No notes found for this movie'
            }
          });
        }
        return res.status(200).json(data[0]);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during getting data from ${TABLE} table`
        }
      }));
  };

  updateMovieNoteByUserId = async (req, res) => {
    const { userId, movieId, note } = req.body;

    return this.sql.query(`UPDATE ${TABLE} SET note = ? WHERE user_id = ? AND movie_id = ?`, [note, userId, movieId])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'Nothing change!'
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during updating note of user'
        }
      }));
  };

  removeMovieNoteByUserId = async (req, res) => {
    const { userId, movieId } = req.query;
    return this.sql.query(`DELETE FROM ${TABLE} WHERE user_id = ? AND movie_id = ?`, [userId, movieId])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No note found!'
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during deleting note of user'
        }
      }));
  };
}

module.exports = UsersNotes;
