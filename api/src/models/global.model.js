const fs = require('fs');

class GlobalModel {
  constructor(database, table, createTableQuery, wantPopulate) {
    this.sql = database;
    this.table = table;
    this.sql.query(createTableQuery);
    if (wantPopulate) {
      this.sql.query(`SELECT EXISTS(SELECT 1 FROM ${this.table}) AS output`)
        .then((rows) => {
          if (rows[0].output === 0) this.populate();
        });
    }
  }

  add = async (req, res) => {
    let body = [];
    let fields;
    if (req.body.constructor === Array) {
      fields = Object.keys(req.body[0]);
      body.push(req.body.map((actor) => Object.values(actor)));
    } else {
      fields = Object.keys(req.body);
      body.push(Object.values(req.body));
      body = [body];
    }

    let query = `INSERT INTO ${this.table} (`;
    fields.forEach((fieldName) => {
      query += fieldName.split(/(?=[A-Z])/).join('_').toLowerCase();
      if (fields[fields.length - 1] !== fieldName) query += ', ';
    });
    query += ') VALUES ?';

    return this.sql.query(query, body)
      .then((rows) => {
        if (res) {
          return res.status(201).json({ id: rows.insertId });
        }
        return null;
      }).catch((err) => {
        if (res) {
          return res.status(400).json({
            error: {
              code: err.code,
              message: `An error occured during insert data in ${this.table} table`
            }
          });
        }
        return null;
      });
  };

  findById = async (req, res) => {
    const { id } = req.params;
    const idColumn = await this.findPrimaryColumnName();

    return this.sql.query(`SELECT * FROM ${this.table} WHERE ${idColumn} = ?`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: `No data found in ${this.table} table`
            }
          });
        }
        return res.status(200).json(data[0]);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during getting data from ${this.table} table`
        }
      }));
  };

  findAll = async (req, res) => {
    const { limit } = req.query;

    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }
    return this.sql.query(`SELECT * FROM ${this.table} ${limit ? `LIMIT ${limit}` : ''}`)
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No plans found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during getting all data from ${this.table} table`
        }
      }));
  };

  updateById = async (req, res) => {
    const { id } = req.params;
    const fields = Object.keys(req.body);
    const idColumn = await this.findPrimaryColumnName();

    let query = `UPDATE ${this.table} SET`;
    fields.forEach((fieldName) => {
      query += ` ${fieldName} = ?`;
      if (fields[fields.length - 1] !== fieldName) query += ',';
    });
    query += ` WHERE ${idColumn} = ?`;

    return this.sql.query(query, [...Object.values(req.body), id])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: `No data found to update with your specifications in ${this.table} table!`
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during updating data from ${this.table} table`
        }
      }));
  };

  removeById = async (req, res) => {
    const { id } = req.params;
    const idColumn = await this.findPrimaryColumnName();

    return this.sql.query(`DELETE FROM ${this.table} WHERE ${idColumn} = ?`, [id])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: `Nothing with your specifications can be removed in ${this.table} table`
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during deleting data from ${this.table} table`
        }
      }));
  };

  populate = async (_, res) => {
    const fileName = this.table.toLowerCase().replace(/([-_][a-z])/g, (group) => group
      .toUpperCase()
      .replace('-', '')
      .replace('_', ''));
    const rawdata = fs.readFileSync(`${__dirname}/../data/${fileName}.json`);
    const data = JSON.parse(rawdata);

    this.add({ body: data }, res);
  };

  findPrimaryColumnName = async () => this.sql.query(`SELECT column_name
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = '${this.table}'
    ORDER BY ordinal_position
    LIMIT 1;`).then((data) => data[0].column_name);
}

module.exports = GlobalModel;
