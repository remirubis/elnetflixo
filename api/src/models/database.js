const mysql = require('mysql');

// Create a connection to the database
const defaultConfig = {
  host: process.env.DB_HOST,
  user: 'root',
  password: process.env.DB_ROOT_PASSWORD,
  database: process.env.DB_NAME
};

class Database {
  constructor(config = defaultConfig) {
    this.connection = mysql.createConnection(config);
  }

  query(sql, args = []) {
    return new Promise((resolve, reject) => {
      this.connection.query(sql, args, (err, rows) => {
        if (err) return reject(err);
        return resolve(rows);
      });
    });
  }

  close() {
    return new Promise((resolve, reject) => {
      this.connection.end((err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  }
}

module.exports = Database;
