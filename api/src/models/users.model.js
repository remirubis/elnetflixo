const crypto = require('crypto');
const GlobalModel = require('./global.model');

const UsersNotes = require('./usersNotes.model');

const TABLE = 'users';

class Users extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        user_id INT AUTO_INCREMENT PRIMARY KEY,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        picture VARCHAR(100) NULL,
        lastname VARCHAR(100) NULL,
        firstname VARCHAR(100) NULL,
        address VARCHAR(150) NULL,
        city VARCHAR(100) NULL,
        zipcode VARCHAR(10),
        plan_id INT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        UNIQUE KEY UNIQUE_USER_EMAIL (email),
        FOREIGN KEY(plan_id) REFERENCES plans(plan_id)
        ) ENGINE=INNODB`,
      true
    );
    new UsersNotes(database);
  }

  add = async (req, res) => {
    let body = [];
    let fields;
    if (req.body.constructor === Array) {
      fields = Object.keys(req.body[0]);
      req.body.forEach((user, i) => {
        if (user.password) req.body[i].password = this.encryptPassword(user.password);
      });
      body.push(req.body.map((actor) => Object.values(actor)));
    } else {
      fields = Object.keys(req.body);
      if (req.body.password) req.body.password = this.encryptPassword(req.body.password);
      body.push(Object.values(req.body));
      body = [body];
    }

    let query = `INSERT INTO ${TABLE} (`;
    fields.forEach((fieldName) => {
      query += fieldName.split(/(?=[A-Z])/).join('_').toLowerCase();
      if (fields[fields.length - 1] !== fieldName) query += ', ';
    });
    query += ') VALUES ?';

    return this.sql.query(query, body)
      .then((rows) => {
        if (res) {
          return res.status(201).json({ id: rows.insertId });
        }
        return null;
      }).catch((err) => {
        if (res) {
          return res.status(400).json({
            error: {
              code: err.code,
              message: 'An error occured during insert user'
            }
          });
        }
        return null;
      });
  };

  findPasswordByEmail = async (req, res) => {
    const { email } = req.query;
    return this.sql.query(`SELECT user_id, password FROM ${TABLE} WHERE email = ?`, [email])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No user found!'
            }
          });
        }
        return res.status(200).json(data[0]);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting user'
        }
      }));
  };

  hashPassword = async (req, res) => {
    const { password } = req.body;
    return res.status(200).json({ password: this.encryptPassword(password) });
  };

  findById = async (req, res) => {
    const { id } = req.params;
    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT plan_id, name, description, price FROM plans) p USING (plan_id) WHERE user_id = ?`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No user found!'
            }
          });
        }
        return res.status(200).json(data[0]);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting user'
        }
      }));
  };

  findAll = async (req, res) => {
    const { limit } = req.query;

    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }

    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT plan_id, name FROM plans) p USING (plan_id) ${limit ? `LIMIT ${limit}` : ''}`)
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No users found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting all users'
        }
      }));
  };

  updateById = async (req, res) => {
    const { id } = req.params;
    const fields = Object.keys(req.body);

    let query = `UPDATE ${TABLE} SET`;
    fields.forEach((fieldName) => {
      query += ` ${fieldName} = ?`;
      if (fields[fields.length - 1] !== fieldName) query += ',';
    });
    query += ' WHERE user_id = ?';

    if (fields.includes('password')) {
      req.body.password = this.encryptPassword(req.body.password);
    }

    return this.sql.query(query, [...Object.values(req.body), id])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'Nothing change!'
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during updating user'
        }
      }));
  };

  encryptPassword = (password) => {
    const salt = 'SdsJu9!@*dsd93D20-2';
    const hash = crypto.createHmac('sha512', salt)
      .update(password)
      .digest('base64');
    const hash2 = crypto.createHmac('sha512', salt)
      .update(`${salt}$${hash}`)
      .digest('base64');
    return hash2;
  };
}

module.exports = Users;
