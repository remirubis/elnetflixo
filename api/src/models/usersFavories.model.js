const GlobalModel = require('./global.model');

const TABLE = 'users_favories';

class UsersFavories extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_id INT NOT NULL,
        movie_id INT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE,
        FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE,
        UNIQUE KEY UNIQUE_LINK (user_id, movie_id)
      ) ENGINE=INNODB`,
      true
    );
  }

  findMoviesByUserId = async (req, res) => {
    const { id } = req.params;
    const { limit } = req.query;

    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }

    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT * FROM movies) p USING (movie_id) WHERE user_id = ? ORDER BY ${TABLE}.created_at DESC ${limit ? `LIMIT ${limit}` : ''}`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No data found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting data'
        }
      }));
  };

  isUserFavMovie = async (req, res) => {
    const { userId, movieId } = req.query;

    return this.sql.query(`SELECT id FROM ${TABLE} WHERE user_id = ? AND movie_id = ?`, [userId, movieId])
      .then((data) => {
        if (!data.length) {
          return res.status(200).json({ isExist: false });
        }
        return res.status(200).json({ isExist: true });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting data'
        }
      }));
  };

  remove = async (req, res) => {
    const { userId, movieId } = req.body;

    return this.sql.query(`DELETE FROM ${TABLE} WHERE user_id = ? AND movie_id = ?`, [parseInt(userId, 10), movieId])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No data found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting data'
        }
      }));
  };
}

module.exports = UsersFavories;
