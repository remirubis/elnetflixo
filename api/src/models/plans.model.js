const GlobalModel = require('./global.model');

const TABLE = 'plans';

class Plans extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        plan_id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        description VARCHAR(250) NULL,
        price FLOAT NOT NULL,
        duration TIME NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        UNIQUE(name)
      ) ENGINE=INNODB`,
      true
    );
  }
}

module.exports = Plans;
