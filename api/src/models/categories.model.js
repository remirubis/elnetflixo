const GlobalModel = require('./global.model');

const TABLE = 'categories';

class Categories extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        category_id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(40) NOT NULL,
        description VARCHAR(250) NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        UNIQUE KEY UNIQUE_CATEGORY_NAME (name)
      ) ENGINE=INNODB`,
      true
    );
  }
}

module.exports = Categories;
