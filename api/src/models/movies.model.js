const GlobalModel = require('./global.model');
const MoviesActorsModel = require('./moviesActors.model');

const TABLE = 'movies';

class Movies extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        movie_id INT AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(255) NOT NULL,
        description TEXT NULL,
        release_date DATETIME NULL,
        duration TIME NOT NULL,
        file VARCHAR(255) NOT NULL,
        picture VARCHAR(100) NULL,
        classification_id INT NULL,
        category_id INT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        UNIQUE(title, picture),
        FOREIGN KEY(category_id) REFERENCES categories(category_id),
        FOREIGN KEY(classification_id) REFERENCES classifications(classification_id) ON DELETE SET NULL
      ) ENGINE=INNODB`,
      true
    );
    this.moviesActorsModel = new MoviesActorsModel(database);
  }

  add = async (req, res) => {
    let body = [];
    let fields;
    const moviesActors = [];

    if (req.body.constructor === Array) {
      let round = 0;
      req.body.forEach((movies) => {
        moviesActors.push(movies.actors);
        delete req.body[round].actors;
        round += 1;
      });
      fields = Object.keys(req.body[0]);
      body.push(req.body.map((movie) => Object.values(movie)));
    } else {
      moviesActors.push(req.body.actors);
      delete req.body.actors;
      fields = Object.keys(req.body);
      body.push(Object.values(req.body));
      body = [body];
    }

    let query = `INSERT INTO ${TABLE} (`;
    fields.forEach((fieldName) => {
      query += fieldName.split(/(?=[A-Z])/).join('_').toLowerCase();
      if (fields[fields.length - 1] !== fieldName) query += ', ';
    });
    query += ') VALUES ?';

    return this.sql.query(query, body)
      .then((rows) => {
        const actorsMoviebody = [];
        moviesActors.forEach((actors, i) => {
          actors.forEach((actor) => {
            const payload = {
              movieId: rows.insertId + i,
              actorId: actor.id,
              role: actor.role ? actor.role : null
            };
            actorsMoviebody.push(payload);
          });
        });

        this.moviesActorsModel.addActor({ body: actorsMoviebody });
        if (res) {
          return res.status(201).json({ id: rows.insertId });
        }
        return null;
      }).catch((err) => {
        if (res) {
          return res.status(400).json({
            error: {
              code: err.code,
              message: 'An error occured during insert movie'
            }
          });
        }
        return null;
      });
  };

  findById = async (req, res) => {
    const { id } = req.params;
    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT category_id, name AS category_name FROM categories) p USING (category_id) WHERE movie_id = ?`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(400).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No movie found!'
            }
          });
        }
        return res.status(200).json({ ...data[0] });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting movie'
        }
      }));
  };

  findByCategoryId = async (req, res) => {
    const { id } = req.params;
    const { limit } = req.query;
    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(400).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }
    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT category_id, name AS category_name FROM categories) p USING (category_id) WHERE category_id = ? ${limit ? `LIMIT ${limit}` : ''}`, [id])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No movie found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting movie'
        }
      }));
  };

  findAll = async (req, res) => {
    const { limit } = req.query;
    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }
    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT category_id, name AS category_name FROM categories) p USING (category_id) ${limit ? `LIMIT ${limit}` : ''}`)
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No movies found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting all movies'
        }
      }));
  };

  updateById = async (req, res) => {
    const { id } = req.params;
    const fields = Object.keys(req.body);
    const actors = [];

    let round = 0;
    req.body.forEach((movies) => {
      actors.push(movies.actors);
      delete req.body[round].actors;
      round += 1;
    });

    let query = `UPDATE ${TABLE} SET`;
    fields.forEach((fieldName) => {
      query += ` ${fieldName} = ?`;
      if (fields[fields.length - 1] !== fieldName) query += ',';
    });
    query += ' WHERE movie_id = ?';

    return this.sql.query(query, [...Object.values(req.body), id])
      .then((rows) => {
        if (!rows.affectedRows) {
          return res.status(400).json({
            message: 'Nothing change!'
          });
        }
        if (fields.includes('actors')) {
          const actorsMoviebody = [];
          actors.forEach((movie) => {
            movie.forEach((actor) => {
              const payload = {
                movieId: rows.insertId,
                actorId: actor.id,
                role: actor.role ? actor.role : null
              };
              actorsMoviebody.push(payload);
            });
          });
          this.moviesActorsModel.add(actorsMoviebody, res);
        }
        if (res) {
          return res.status(201).json({
            message: 'Movie added!',
            data: {
              id: rows.insertId
            }
          });
        }
        return null;
      }).catch((err) => res.status(400).json({
        message: 'An error occured during updating movie',
        error: {
          code: err.code,
          message: err.sqlMessage
        }
      }));
  };

  findRecentsMovies = async (req, res) => {
    const { limit } = req.query;
    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }
    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT category_id, name AS category_name FROM categories) p USING (category_id) ORDER BY created_at DESC ${limit ? `LIMIT ${limit}` : ''}`)
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No movie found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting movies'
        }
      }));
  };

  searchMovie = async (req, res) => {
    const { searchValue, limit } = req.query;
    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }
    return this.sql.query(`
      SELECT * FROM movies
        LEFT JOIN (SELECT category_id, name AS category_name FROM categories) p USING (category_id)
        WHERE (title LIKE '%${searchValue}%' OR category_name LIKE '%${searchValue}%')
        ORDER BY updated_at DESC
      ${limit ? `LIMIT ${limit}` : ''}
    `)
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No movie found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting movies'
        }
      }));
  };
}

module.exports = Movies;
