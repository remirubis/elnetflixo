const GlobalModel = require('./global.model');

const TABLE = 'movies_actors';

class MoviesActors extends GlobalModel {
  constructor(database) {
    super(
      database,
      TABLE,
      `CREATE TABLE IF NOT EXISTS ${TABLE} (
        id INT AUTO_INCREMENT PRIMARY KEY,
        movie_id INT NOT NULL,
        actor_id INT NOT NULL,
        role VARCHAR(100) NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE,
        FOREIGN KEY(actor_id) REFERENCES actors(actor_id) ON DELETE CASCADE,
        UNIQUE KEY UNIQUE_LINK (movie_id, actor_id)
      ) ENGINE=INNODB`,
      false
    );
  }

  addActor = async (req) => {
    let body = [];
    let fields;
    if (req.body.constructor === Array) {
      fields = Object.keys(req.body[0]);
      body.push(req.body.map((actor) => Object.values(actor)));
    } else {
      fields = Object.keys(req.body);
      body.push(Object.values(req.body));
      body = [body];
    }

    let query = `INSERT INTO ${this.table} (`;
    fields.forEach((fieldName) => {
      query += fieldName.split(/(?=[A-Z])/).join('_').toLowerCase();
      if (fields[fields.length - 1] !== fieldName) query += ', ';
    });
    query += ') VALUES ?';

    return this.sql.query(query, body);
  };

  findActorsByMovieId = async (req, res) => {
    const { movie } = req.params;
    const { limit } = req.query;

    if (limit && !Number.isInteger(Number(limit))) {
      return res.status(412).json({
        error: {
          code: 'TYPE',
          message: 'Invalid parameter type'
        }
      });
    }

    return this.sql.query(`SELECT * FROM ${TABLE} LEFT JOIN (SELECT * FROM actors) p USING (actor_id) WHERE movie_id = ? ${limit ? `LIMIT ${limit}` : ''}`, [movie])
      .then((data) => {
        if (!data.length) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: 'No data found!'
            }
          });
        }
        return res.status(200).json(data);
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: 'An error occured during getting data'
        }
      }));
  };

  updateNoteByMovieIdActorId = async (req, res) => {
    const { movieId, actorId } = req.query;

    const query = `UPDATE ${this.table} SET role = ? WHERE movie_id = ? AND actor_id = ?`;

    return this.sql.query(query, [req.body.role, movieId, actorId])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: `No data found to update with your specifications in ${this.table} table!`
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during updating data from ${this.table} table`
        }
      }));
  };

  removeActorOfMovie = async (req, res) => {
    const { movieId, actorId } = req.query;

    return this.sql.query(`DELETE FROM ${this.table} WHERE movie_id = ? AND actor_id = ?`, [movieId, actorId])
      .then((data) => {
        if (!data.affectedRows) {
          return res.status(404).json({
            error: {
              code: 'NOT-EXIST',
              message: `Nothing with your specifications can be removed in ${this.table} table`
            }
          });
        }
        return res.status(200).json({ id: data.insertId });
      }).catch((err) => res.status(400).json({
        error: {
          code: err.code,
          message: `An error occured during deleting data from ${this.table} table`
        }
      }));
  };
}

module.exports = MoviesActors;
