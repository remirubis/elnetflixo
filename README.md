# El Netflixo

Create an online streaming plateform like Netflix with some specifications

## Setup project

For launching this project with MySQL database please follow this steps :

1. Copy and past .env.sample file and rename it to .env
2. Follow steps in .env.sample file for configure correctly MySQL server
3. Example:

```dosini
# MySQL configurations
MYSQL_SERVER_PORT=6201
MYSQL_ROOT_PASSWORD=JqFy4y53Po&LCc56
MYSQL_DATABASE=ElNetflixo_xo90Ad
```

## ✅  Launch project

```bash
docker-compose up -d
```

**REQUIRED** This project use docker for create containers, [please install it before launch](https://www.docker.com/get-started).

After few seconds you can access to plateform at:

- Vue front: [http://localhost:8080](http://localhost:8080)
- PhpMyAdmin: [http://localhost:8180](http://localhost:8180)

### See container logs

- Log of MySQL server

```bash
docker logs -f mysql
```

- Log of PhpMyAdmin

```bash
docker logs -f phpmyadmin
```

- Log of vue service

```bash
docker logs -f vue-app
```

## 🔧 Functionalities

- Users (Login, Register)
- Dashboard
- Search a movie
- Router
- SASS support

## 🚨  Dependencies

- [sass-loader](https://www.npmjs.com/package/sass-loader)
- [vue-router](https://www.npmjs.com/package/vue-router)
- [vuex](https://www.npmjs.com/package/vuex)

## 👤 Creator

[Rémi RUBIS](https://gitlab.com/remirubis)
